import renderEducation from './render/renderEducation';
import renderAboutMe from './render/renderAboutMe';
import renderHeader from './render/renderHeader';
import fetchData from './utils/fetchData';
import createPerson from './utils/createPerson';

fetchData()
  .then(item => {
    const person = createPerson(item);
    const { name, age, description, educations } = person;
    renderHeader(name, age);
    renderAboutMe(description);
    renderEducation(educations);
  })
  .catch(error => new Error(error));
