import $ from 'jquery';

export default function renderAboutMe(description) {
  $('#person-description').html(description);
}
