import $ from 'jquery';

export default function renderHeader(name, age) {
  $('#name').html(name);
  $('#age').html(age);
}
