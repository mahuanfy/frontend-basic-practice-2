import $ from 'jquery';

export default function renderEducation(educations) {
  const _html = $('.education');
  educations.forEach(education => {
    _html.append(`<li>
          <span>${education.year}</span>
          <div>
            <h4>${education.title}</h4>
            <p>${education.description}</p>
          </div>
        </li>`);
  });
}
