export default function fetchData() {
  const url = 'http://localhost:3000/person';
  return fetch(url).then(response => response.json());
}
