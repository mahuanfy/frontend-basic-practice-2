import { Person } from '../resources/person';

export default function createPerson(item) {
  const { name, age, description, educations } = item;
  return new Person(name, age, description, educations);
}
